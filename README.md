
# Human Development Index (HDI) of Brazilian Cities

This project consists of a study on the Human Development Index (HDI or IDHM) of Brazilian cities. HDI is a composite measure that seeks to assess the level of human development in a given region, taking into account indicators of longevity, education, and income.

The main objective of this study is to analyze and predict the HDI of Brazilian cities based on a set of variables and socioeconomic characteristics.

![HDI distribution in Brazilian cities.](./plots/IDHM_map.png)

## Project Steps

1.  Exploratory Data Analysis: Initially, an exploratory data analysis was conducted to understand the distribution, trends, and relationships between the variables. This included identifying missing values, visualizing distributions, and analyzing correlations.
    
2.  Data Preprocessing: Next, data preprocessing techniques were applied, such as handling missing values, normalizing or standardizing variables, dimensionality reduction, among others. The goal was to prepare the data for feeding into the prediction models.
	
	![Representation of variance for several reduced dimensionality](./plots/dim.png)
	
3.  Model Building: The Random Forest model was used to build a prediction model for HDI.
    
4.  Evaluation and Interpretation of Results: The model's performance was evaluated using the R2 metric, achieving a performance of 0.8. Additionally, the distribution of errors was observed in a plot of HDI vs. HDI_predict.
	
	![Representation](./plots/score.png)

## How to Use this Project

This project is organized into different files and directories:

-   `/1-Brazil-Cities.ipynb`: Contains Jupyter notebooks used to perform the analyses and build the models. Items:
	- Imports 
	- Functions 
	- Load data 
	- Exploratory Analysis
		- map 
		- Drop missing data and formatting 
		- One hot encode 
		- Dimensionality reduction
	- Model
    
-   `plots/`: Contains the obtained plots also present in the notebook.
    

## Requirements

-   Python 3.x
-   [Jupyter](https://jupyter.org/install)
-   Python libraries: pandas, numpy, scikit-learn, matplotlib, seaborn, geobr.

## References

-   [Brazilian Cities - Kaggle](https://www.kaggle.com/datasets/crisparada/brazilian-cities/code)

